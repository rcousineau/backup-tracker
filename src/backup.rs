use git2::{Direction, IndexAddOption, ObjectType, Oid, Repository, ResetType, Signature};
use std::collections::BTreeMap;
use std::error::Error;
use std::path::PathBuf;
use toml::Value;

#[derive(Debug)]
pub struct Backup {
    pub name: String,
    pub src: String,
    pub dest: String,
}

impl Backup {
    pub fn new(name: String, src: String, dest: String, init: bool) -> Backup {
        if init {
            println!("[debug] initializing git repository in {}", src.as_str());
            let src_repo = Repository::init(src.clone()).expect("failed to create src repo");

            let dest_repo_path: PathBuf = [dest.clone(), format!("{}.git", name.clone())]
                .iter()
                .collect();

            println!(
                "[debug] initializing bare git repository {:?}",
                dest_repo_path.as_path()
            );
            Repository::init_bare(dest_repo_path.as_path()).expect("failed to create dest repo");

            let dest_repo_path = dest_repo_path
                .to_str()
                .expect("failed to convert dest repo path to string");

            src_repo
                .remote("backup", dest_repo_path)
                .expect("failed to add remote to dest repo");
            src_repo
                .remote_add_push("backup", "refs/heads/master:refs/heads/master")
                .expect("failed to add ref to dest repo");
        }
        Backup { name, src, dest }
    }

    pub fn summary(&self) -> String {
        format!("{}: {} -> {}", self.name, self.src, self.dest)
    }

    pub fn to_toml(&self) -> (String, Value) {
        let mut backup_table = BTreeMap::new();
        backup_table.insert(String::from("src"), Value::String(self.src.clone()));
        backup_table.insert(String::from("dest"), Value::String(self.dest.clone()));
        (self.name.clone(), Value::Table(backup_table))
    }

    pub fn status(&self) {
        let src_repo = self.src_repo();
        let statuses = src_repo.statuses(None);
        match statuses {
            Ok(statuses) => {
                statuses.iter().for_each(|entry| {
                    let path = entry.path().expect("failed to get path for status entry");
                    println!("{:?}: {}", entry.status(), path);
                });
                println!();
            }
            _ => {
                println!("failed to retrieve src repo status...");
            }
        }
    }

    pub fn add_file(&self, filename: String) -> Result<(), Box<dyn Error>> {
        let src_repo = self.src_repo();
        let mut index = src_repo.index()?;
        index.add_all(vec![&filename], IndexAddOption::empty(), None)?;
        let oid = index.write_tree()?;
        let author = Signature::now("backup-tracker", "backup-tracker@example.net")?;
        let message = format!("add/update {}", &filename);
        let tree = src_repo.find_tree(oid)?;

        let new_commit_oid: Option<Oid>;

        if src_repo.head().is_ok() {
            // add commit onto HEAD
            let parent = src_repo
                .head()?
                .resolve()?
                .peel(ObjectType::Commit)?
                .into_commit()
                .expect("failed to get HEAD from src repo");

            let oid =
                src_repo.commit(Some("HEAD"), &author, &author, &message, &tree, &[&parent])?;

            new_commit_oid = Some(oid);
        } else {
            // this is the first commit
            let oid = src_repo.commit(Some("HEAD"), &author, &author, &message, &tree, &[])?;

            new_commit_oid = Some(oid);
        }

        let new_commit_oid = new_commit_oid.expect("failed to create new commit in src repo");

        // do a reset to fix working tree
        let new_commit = src_repo.find_commit(new_commit_oid)?;
        src_repo.reset(new_commit.as_object(), ResetType::Mixed, None)?;

        let mut remote = src_repo.find_remote("backup")?;
        remote.connect(Direction::Push)?;
        remote.push(&["refs/heads/master:refs/heads/master"], None)?;
        Ok(())
    }

    fn src_repo(&self) -> Repository {
        Repository::open(self.src.clone()).expect("failed to open src repo")
    }
}
