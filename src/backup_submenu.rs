use crate::menu::Menu;
use crate::Backup;

pub struct BackupSubmenu<'a> {
    backup: &'a Backup,
}

impl<'a> BackupSubmenu<'a> {
    pub fn new(backup: &'a Backup) -> BackupSubmenu {
        BackupSubmenu { backup }
    }

    pub fn start(&self) {
        self.print_menu();

        loop {
            let command = Menu::prompt(String::from("> "));
            match self.invoke_command(command) {
                Ok(_) => continue,
                Err(_) => break,
            }
        }
    }

    fn print_menu(&self) {
        let summary = self.backup.summary();
        println!("{}", summary);
        println!("{}", "=".repeat(summary.len()));

        println!("Commands");
        println!("--------");

        println!("s. status");
        println!("a. add file(s)");
        println!("b. back");
    }

    fn invoke_command(&self, command: String) -> Result<(), String> {
        match command.trim() {
            "s" => {
                self.backup.status();
                self.print_menu();
                Ok(())
            }
            "a" => {
                let filename = Menu::prompt(String::from("filename: "));
                if let Err(error) = self.backup.add_file(String::from(filename.trim())) {
                    println!("Error adding file. {}", error.description());
                }
                self.print_menu();
                Ok(())
            }
            "b" => Err(String::from("back")),
            _ => Ok(()),
        }
    }
}
