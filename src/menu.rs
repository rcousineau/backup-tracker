use std::io;
use std::io::prelude::*;

pub struct Menu;

impl Menu {
    pub fn prompt(message: String) -> String {
        print!("{}", message.as_str());

        // flush the message to the screen (no auto-flush without newline)
        io::stdout().flush().ok().expect("failed to flush stdout");

        let mut input = String::new();

        io::stdin()
            .read_line(&mut input)
            .expect("failed to read line from stdin");

        input
    }
}
