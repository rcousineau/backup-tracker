extern crate backup_tracker;

use backup_tracker::Config;
use backup_tracker::MainMenu;
use std::process;

fn main() {
    match Config::new() {
        Ok(mut config) => {
            let mut menu = MainMenu::new(&mut config);
            menu.start();
        }
        Err(err) => {
            println!("{}", err);
            process::exit(1);
        }
    }
}
