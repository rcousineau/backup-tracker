use crate::backup_submenu::BackupSubmenu;
use crate::menu::Menu;
use crate::Backup;
use crate::Config;

pub struct MainMenu<'a> {
    config: &'a mut Config,
}

impl<'a> MainMenu<'a> {
    pub fn new(config: &'a mut Config) -> MainMenu {
        MainMenu { config }
    }

    pub fn start(&mut self) {
        self.print_menu();

        loop {
            let command = Menu::prompt(String::from("> "));
            match self.invoke_command(command) {
                Ok(_) => continue,
                Err(_) => break,
            }
        }
    }

    fn print_menu(&self) {
        println!("******************");
        println!("* Ƀackup Ŧracker *");
        println!("******************");

        println!("Backups");
        println!("-------");

        println!("{}", self.config.summary());

        println!();

        println!("Commands");
        println!("--------");

        println!("a. add new backup");
        println!("q. quit");
    }

    fn invoke_command(&mut self, command: String) -> Result<(), String> {
        match command.trim() {
            "a" => Ok(self.add_backup()),
            "q" => Err(String::from("quit")),
            idx => {
                let idx: usize = match idx.parse() {
                    Ok(idx) => idx,
                    _ => return Ok(()),
                };
                if idx >= 1 && idx <= self.config.backups().len() {
                    let submenu = BackupSubmenu::new(
                        self.config
                            .backups()
                            .get(idx - 1)
                            .expect("vec bounds already checked"),
                    );
                    // launch submenu
                    submenu.start();
                    // after returning from submenu...
                    self.print_menu();
                    return Ok(());
                }
                Ok(())
            }
        }
    }

    fn add_backup(&mut self) {
        let name = Menu::prompt(String::from("name: "));
        let src = Menu::prompt(String::from("src: "));
        let dest = Menu::prompt(String::from("dest: "));
        self.config.add_backup(Backup::new(
            String::from(name.trim()),
            String::from(src.trim()),
            String::from(dest.trim()),
            true,
        ));
        println!();
        self.print_menu();
    }
}
