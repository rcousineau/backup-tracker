extern crate toml;

use crate::backup::Backup;
use std::collections::BTreeMap;
use std::env;
use std::error::Error;
use std::fs;
use std::io::prelude::*;
use std::path::PathBuf;
use toml::Value;

const CONFIG_FILENAME: &'static str = ".backup_tracker.toml";

#[derive(Debug)]
pub struct Config {
    backups: Vec<Backup>,
}

impl Config {
    pub fn new() -> Result<Config, Box<dyn Error>> {
        match Config::read_from_file() {
            Ok(file_contents) => {
                let config_data = file_contents
                    .as_str()
                    .parse::<Value>()
                    .expect("failed to parse config file");
                let backups = Self::load(config_data);
                Ok(Config { backups })
            }
            Err(e) => Err(e),
        }
    }

    pub fn backups(&self) -> &Vec<Backup> {
        &self.backups
    }

    pub fn summary(&self) -> String {
        let summaries: Vec<String> = self.backups.iter().map(|backup| backup.summary()).collect();

        let summaries: Vec<String> = summaries
            .iter()
            .enumerate()
            .map(|(index, backup)| format!("{}. {}", index + 1, backup))
            .collect();

        summaries.join("\n")
    }

    pub fn add_backup(&mut self, backup: Backup) {
        self.backups.push(backup);
        let contents = toml::to_string(&self.to_toml()).expect("failed to convert config to toml");
        Self::write_to_file(String::from(contents)).expect("failed to write config file");
    }

    fn load(config_data: Value) -> Vec<Backup> {
        match config_data.as_table() {
            Some(table) => table
                .iter()
                .map(|(name, paths)| {
                    let src = paths["src"].as_str().expect("missing 'src' in config");
                    let dest = paths["dest"].as_str().expect("missing 'dest' in config");
                    Backup::new(name.clone(), String::from(src), String::from(dest), false)
                })
                .collect(),
            _ => vec![],
        }
    }

    fn to_toml(&self) -> Value {
        let mut table = BTreeMap::new();
        self.backups.iter().for_each(|backup| {
            let (name, backup_table) = backup.to_toml();
            table.insert(name, backup_table);
        });
        Value::Table(table)
    }

    fn build_file_path() -> PathBuf {
        let dir = env::var("HOME").expect("env var 'HOME' not set");
        [dir, String::from(CONFIG_FILENAME)].iter().collect()
    }

    fn read_from_file() -> Result<String, Box<dyn Error>> {
        let config_file_path = Self::build_file_path();
        println!(
            "[debug] loading config file {:?}",
            config_file_path.as_path()
        );
        {
            // create file if it doesn't exist
            fs::OpenOptions::new()
                .write(true)
                .create(true)
                .open(config_file_path.as_path())?;
        }
        let contents = fs::read_to_string(config_file_path.as_path())?;
        Ok(contents)
    }

    fn write_to_file(contents: String) -> Result<(), Box<dyn Error>> {
        let config_file_path = Self::build_file_path();
        println!(
            "[debug] writing to config file {:?}",
            config_file_path.as_path()
        );
        let mut file = fs::OpenOptions::new()
            .write(true)
            .open(config_file_path.as_path())?;
        file.write(contents.as_bytes())?;
        Ok(())
    }
}
